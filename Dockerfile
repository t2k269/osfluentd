FROM fluent/fluentd:v1.10-debian-1

# Use root account to use apt
USER root

# below RUN includes plugin as examples elasticsearch is not required
# you may customize including plugins as you wish
RUN buildDeps="sudo make gcc g++ libc-dev dstat openssh-client" \
 && apt-get update \
 && apt-get install -y --no-install-recommends $buildDeps

RUN sudo gem install fluent-plugin-systemd \
 && sudo gem install fluent-plugin-dstat \
 && sudo gem install fluent-plugin-top \
 && sudo gem install fluent-plugin-splunk-enterprise \
 && sudo gem install fluent-plugin-rewrite-tag-filter \
 && sudo gem sources --clear-all \
 && SUDO_FORCE_REMOVE=yes \
    apt-get purge -y --auto-remove \
                  -o APT::AutoRemove::RecommendsImportant=false \
                  $buildDeps \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

COPY fluent.conf /fluentd/etc/
COPY entrypoint.sh /bin/

USER fluent

